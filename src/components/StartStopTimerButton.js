import React from "react";
import { Button } from "react-bootstrap";

export const StartStopTimerButton = ({
  status,
  runningSince,
  onTimerStart,
  onTimerStop,
}) => {
  if (runningSince) {
    return (
      <Button
        variant="outline-danger"
        size="sm"
        disabled={status === "completed"}
        onClick={onTimerStop}
      >
        Stop Timer
      </Button>
    );
  }

  return (
    <Button
      variant="outline-success"
      size="sm"
      disabled={status === "completed"}
      onClick={onTimerStart}
    >
      Start Timer
    </Button>
  );
};
