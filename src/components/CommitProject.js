import React, { useRef } from "react";
import { Button, Form } from "react-bootstrap";

export const CommitProject = ({ onSubmit, projectId, onHide }) => {
  const commitMessageRef = useRef();

  function handleAddCommit(evt) {
    evt.preventDefault();
    const commitMessage = commitMessageRef.current.value;
    onSubmit(commitMessage, projectId);
    onHide();
  }
  return (
    <Form onSubmit={handleAddCommit}>
      <Form.Group className="mb-3">
        <Form.Label>Enter your Commit</Form.Label>
        <Form.Control
          type="text"
          placeholder='Done with "your feature name"'
          ref={commitMessageRef}
        />
      </Form.Group>
      <Button type="submit" variant="primary">
        Add Commit
      </Button>
    </Form>
  );
};
