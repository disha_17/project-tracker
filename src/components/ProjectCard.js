import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useState } from "react";
import Helpers from "../Helpers";
import { Badge, Button, Card, Col, Stack } from "react-bootstrap";
import {
  faPencilSquare,
  faTrash,
  faCodeCommit,
  faFileContract,
  onShowCommits,
} from "@fortawesome/free-solid-svg-icons";
import { StartStopTimerButton } from "./StartStopTimerButton";
import { useEffect } from "react";

export const ProjectCard = ({
  project,
  onTimerStart,
  onTimerStop,
  onEdit,
  onDelete,
  onCommit,
  onShowCommits,
  handleShow,
}) => {
  function handleStartTimer() {
    onTimerStart(project.id);
  }
  function handleStopTimer() {
    onTimerStop(project.id);
  }

  function handleCommit() {
    if (project.runningSince) {
      onTimerStop(project.id);
    }
    onCommit(project.id);
  }

  function handleShowCommits() {
    onShowCommits(project.id);
  }
  const [updateCount, setUpdateCount] = useState(0);
  useEffect(() => {
    const interval = setInterval(() => setUpdateCount(updateCount + 1), 100);
    //cleanup
    return () => clearInterval(interval);
  }, [updateCount]);
  return (
    <Col md={3}>
      <Card border={project.status === "completed" ? "success" : "info"}>
        <Card.Header>
          <Stack direction="horizontal">
            <h4 className="me-auto">{project.name} </h4>
            <div className="vr" />
            <p className="my-auto ms-2">${project.price}</p>
          </Stack>
        </Card.Header>
        <Card.Body>
          <Card.Text>
            <span className="d-block">{project.description}</span>
            <h2 className="text-center text-muted">
              {Helpers.renderElapsedString(
                project.elapsed,
                project.runningSince
              )}
            </h2>
          </Card.Text>

          <Stack direction="horizontal" gap={3} className="mt-4">
            <Badge
              pill
              bg={project.status === "completed" ? "success" : "info"}
              size="md"
            >
              {project.status}
            </Badge>
            <Button
              className="ms-auto"
              size="sm"
              variant="outline-info"
              onClick={() => {
                handleCommit(project.id);
              }}
            >
              <FontAwesomeIcon icon={faCodeCommit} />
            </Button>

            <Button
              variant="outline-primary"
              size="sm"
              onClick={handleShowCommits}
            >
              <FontAwesomeIcon icon={faFileContract} />
            </Button>
            <Button
              variant="outline-danger"
              className=""
              size="sm"
              // disabled={project.status === "completed"}
              onClick={() => onDelete(project.id)}
            >
              <FontAwesomeIcon icon={faTrash} />
            </Button>
            <Button
              variant="outline-warning"
              size="sm"
              disabled={project.status === "completed"}
              onClick={() => onEdit(project.id)}
            >
              <FontAwesomeIcon icon={faPencilSquare} />
            </Button>
          </Stack>
          <div className="d-grid mt-2">
            <StartStopTimerButton
              status={project.status}
              runningSince={project.runningSince}
              onTimerStart={handleStartTimer}
              onTimerStop={handleStopTimer}
            />
          </div>
        </Card.Body>
      </Card>
    </Col>
  );
};
