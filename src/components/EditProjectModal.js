import React from "react";
import { Modal } from "react-bootstrap";
import AddUpdateProject from "./AddUpdateProject";
import { CommitProject } from "./CommitProject";

export const EditProjectModal = ({
  show,
  onClose,
  project,
  commitProject,
  onSubmit,
  onAddCommit,
}) => {
  if (!project && !commitProject) {
    return <></>;
  }
  return (
    <Modal show={show} onHide={onClose}>
      <Modal.Header closeButton>
        <Modal.Title>{commitProject ? "Commit" : "Update Project"}</Modal.Title>
      </Modal.Header>

      <Modal.Body>
        {project && (
          <AddUpdateProject
            project={project}
            onHide={onClose}
            onSubmit={onSubmit}
          />
        )}

        {commitProject && (
          <CommitProject
            onSubmit={onAddCommit}
            projectId={commitProject}
            onHide={onClose}
          />
        )}
      </Modal.Body>
    </Modal>
  );
};
