import React from "react";
import { ListGroup } from "react-bootstrap";
export const DisplayCommits = ({ commits }) => {
  function humanizedTime(timeStamp) {
    let date = new Date(timeStamp);
    let hours = date.getHours();
    let min = date.getMinutes();
    let day = date.getDate();
    let month = date.getMonth();
    let year = date.getFullYear();
    let dateFormat =
      "Date:- " +
      day +
      "/" +
      month +
      "/" +
      year +
      " Duration:- " +
      hours +
      ":" +
      min;

    return dateFormat;
  }
  return (
    <div>
      <h4>Total Commits: {commits.length}</h4>
      <ListGroup>
        {commits.map((commit) => (
          <div key={commit.id}>
            <ListGroup.Item>
              <p>Message: {commit.message} </p>
              {humanizedTime(commit.time)}
            </ListGroup.Item>
          </div>
        ))}
      </ListGroup>
    </div>
  );
};
