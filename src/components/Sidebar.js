import React from "react";
import { Offcanvas } from "react-bootstrap";
import AddUpdateProject from "./AddUpdateProject";
import AddProject from "./AddUpdateProject";
import { DisplayCommits } from "./DisplayCommits";

const Sidebar = ({ show, handleClose, onSubmit, onShowCommits, commits }) => {
  return (
    <Offcanvas show={show} onHide={handleClose} placement="end">
      <Offcanvas.Header closeButton>
        <Offcanvas.Title>
          {commits ? "Your Commits History" : "Add Project"}
        </Offcanvas.Title>
      </Offcanvas.Header>

      <Offcanvas.Body>
        {!commits && (
          <AddUpdateProject onSubmit={onSubmit} onHide={handleClose} />
        )}

        {commits && <DisplayCommits commits={commits} />}
      </Offcanvas.Body>
    </Offcanvas>
  );
};

export default Sidebar;
