import "./App.css";
import Button from "react-bootstrap/Button";
import Header from "./components/Header";
import { Badge, Card, Col, Container, Row, Stack } from "react-bootstrap";
import { ProjectCard } from "./components/ProjectCard";
import { v4 as uuid } from "uuid";
import { useState } from "react";
import { EditProjectModal } from "./components/EditProjectModal";
import { useEffect } from "react";

function App() {
  const description =
    "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Nesciunt repellendus enim doloremque nisi similique dolorum laboriosam culpa quisquam nihil id, cupiditate quasi aliquam deserunt molestiae, quis recusandae eos veritatis? Error!";
  // const projects = [
  // { id: 1, name: "Project 1", price: "45", description, status: "ongoing" },
  // { id: 2, name: "Project 2", price: "65", description, status: "ongoing" },
  // { id: 3, name: "Project 2", price: "65", description, status: "completed" },
  // ];

  const [modalShow, setModalShow] = useState(false);
  const [editProject, setEditProject] = useState(null);
  const [commitProject, setCommitProject] = useState(null);
  const [showCommits, setShowCommits] = useState(null);

  const [projects, setProjects] = useState(
    JSON.parse(localStorage.getItem("projects")) || []
  );

  useEffect(() => {
    localStorage.setItem("projects", JSON.stringify(projects));
  }, [projects]);

  function handleModalClose() {
    setModalShow(false);
    setEditProject(null);
    setCommitProject(null);
  }

  function handleCommit(projectId) {
    setModalShow(true);
    console.log("Commit:- " + projectId);
    for (let i = 0; i < projects.length; i++) {
      if (projects[i].id === projectId) {
        setCommitProject(projects[i]);
        break;
      }
    }
  }
  
  function handleAddCommit(commit, project) {
    commit = {
      id: uuid(),
      message: commit,
      time: Date.now(),
    };
    projects.map((cproject) => {
      if (cproject.id == project.id) {
        return {
          ...cproject,
          commits: cproject.commits.push(commit),
        };
      }
      return cproject;
    });

    // console.log(projects);
  }

  function handleShowCommits(projectId) {
    console.log("Project Id: " + projectId);

    projects.map((project) => {
      if (project.id == projectId) {
        let filteredCommits = [];
        let commits = project.commits;
        for (let commit in commits) {
          filteredCommits.push({
            id: commits[commit].id,
            message: commits[commit].message,
            time: commits[commit].time,
          });
        }
        setShowCommits(filteredCommits);
      }
      return project;
    });
  }
  function handleAddProject(project) {
    const newProject = {
      id: uuid(),
      name: project.name,
      description: project.description,
      price: project.price,
      status: project.status,
      elapsed: 0,
      runningSince: null,
      commits: [],
    };
    setProjects([...projects, newProject]);
  }

  function handleStartTimer(projectId) {
    setProjects(
      projects.map((project) => {
        if (project.id === projectId) {
          return { ...project, runningSince: Date.now() };
        }
        return project;
      })
    );
  }

  function handleStopTimer(projectId) {
    setProjects(
      projects.map((project) => {
        if (project.id === projectId) {
          const totalElapsed =
            project.elapsed + (Date.now() - project.runningSince);
          return { ...project, runningSince: null, elapsed: totalElapsed };
        }
        return project;
      })
    );
  }

  function handleUpdateProject(updatedProject, projectId) {
    setProjects(
      projects.map((project) => {
        if (project.id === projectId) {
          return {
            /*We did this because here it might change ellapse timings */
            ...project,
            name: updatedProject.name,
            description: updatedProject.description,
            price: updatedProject.price,
            status: updatedProject.status,
          };
        }
        return project;
      })
    );
  }

  function handleEdit(projectId) {
    setModalShow(true);

    for (let i = 0; i < projects.length; i++) {
      if (projects[i].id === projectId) {
        setEditProject(projects[i]);
        break;
      }
    }
  }

  function handleDelete(projectId) {
    setProjects(projects.filter((project) => project.id !== projectId));
  }
  return (
    <>
      <Header
        onSubmit={handleAddProject}
        commits={showCommits}
        setShowCommits={setShowCommits}
      />
      <Container className="mt-3">
        <Row>
          {projects.map((project) => (
            <ProjectCard
              key={project.id}
              project={project}
              onTimerStart={handleStartTimer}
              onTimerStop={handleStopTimer}
              onEdit={handleEdit}
              onDelete={handleDelete}
              onCommit={handleCommit}
              onShowCommits={handleShowCommits}
            />
          ))}
        </Row>
        <EditProjectModal
          show={modalShow}
          onClose={handleModalClose}
          project={editProject}
          commitProject={commitProject}
          onSubmit={handleUpdateProject}
          onAddCommit={handleAddCommit}
        />
      </Container>
    </>
  );
}

export default App;
